﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FarmacyAPI.Services.Contracts
{
    public interface ILoggerService
    {
        void logInfo(string message);
        void logWarn(string message);
        void logDebug(string message);
        void logError(string message);
    }
}
