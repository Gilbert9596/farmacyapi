USE [master]
GO
/****** Object:  Database [Farmacy]    Script Date: 14/05/2020 09:58:22 PM ******/
CREATE DATABASE [Farmacy]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Farmacy', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Farmacy.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Farmacy_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Farmacy_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Farmacy] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Farmacy].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Farmacy] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Farmacy] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Farmacy] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Farmacy] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Farmacy] SET ARITHABORT OFF 
GO
ALTER DATABASE [Farmacy] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Farmacy] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Farmacy] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Farmacy] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Farmacy] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Farmacy] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Farmacy] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Farmacy] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Farmacy] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Farmacy] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Farmacy] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Farmacy] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Farmacy] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Farmacy] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Farmacy] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Farmacy] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Farmacy] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Farmacy] SET RECOVERY FULL 
GO
ALTER DATABASE [Farmacy] SET  MULTI_USER 
GO
ALTER DATABASE [Farmacy] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Farmacy] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Farmacy] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Farmacy] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Farmacy] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Farmacy', N'ON'
GO
ALTER DATABASE [Farmacy] SET QUERY_STORE = OFF
GO
USE [Farmacy]
GO
/****** Object:  Table [dbo].[OrderDetail]    Script Date: 14/05/2020 09:58:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetail](
	[idOrderDetail] [int] IDENTITY(1,1) NOT NULL,
	[idOrderHeader] [int] NULL,
	[idProducto] [int] NULL,
	[cantidad] [int] NULL,
	[precioUnidad] [money] NULL,
	[precioTotal] [money] NULL,
	[itbis] [money] NULL,
PRIMARY KEY CLUSTERED 
(
	[idOrderDetail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderHeader]    Script Date: 14/05/2020 09:58:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderHeader](
	[idOrderHeader] [int] IDENTITY(1,1) NOT NULL,
	[idUsuario] [int] NULL,
	[subtotal] [money] NULL,
	[totalVenta] [money] NULL,
	[fechaVenta] [datetime] NULL,
	[posicion] [int] NULL,
 CONSTRAINT [PK__OrderHea__113CCB4B3E890DB7] PRIMARY KEY CLUSTERED 
(
	[idOrderHeader] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Permisos]    Script Date: 14/05/2020 09:58:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permisos](
	[idPermiso] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](200) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idPermiso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Productos]    Script Date: 14/05/2020 09:58:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Productos](
	[idProducto] [int] IDENTITY(1,1) NOT NULL,
	[idProveedor] [int] NULL,
	[descripcion] [varchar](200) NOT NULL,
	[precio] [money] NULL,
	[fechaVencimiento] [date] NULL,
	[stock] [int] NULL,
	[state] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[idProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Proveedores]    Script Date: 14/05/2020 09:58:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Proveedores](
	[idProveedor] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](200) NULL,
	[telefono] [varchar](11) NULL,
	[direccion] [varchar](200) NULL,
	[cedula] [varchar](11) NULL,
	[rnc] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[idProveedor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 14/05/2020 09:58:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[idUsuario] [int] IDENTITY(1,1) NOT NULL,
	[idPermiso] [int] NULL,
	[nombreUsuario] [varchar](60) NOT NULL,
	[nombreCompleto] [varchar](200) NOT NULL,
	[email] [varchar](200) NULL,
	[contraseña] [varchar](60) NOT NULL,
	[state] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [EncabezadoDetalleOrden] FOREIGN KEY([idOrderHeader])
REFERENCES [dbo].[OrderHeader] ([idOrderHeader])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [EncabezadoDetalleOrden]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [ProductoDetalle] FOREIGN KEY([idProducto])
REFERENCES [dbo].[Productos] ([idProducto])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [ProductoDetalle]
GO
ALTER TABLE [dbo].[OrderHeader]  WITH CHECK ADD  CONSTRAINT [UsuarioVenta] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuarios] ([idUsuario])
GO
ALTER TABLE [dbo].[OrderHeader] CHECK CONSTRAINT [UsuarioVenta]
GO
ALTER TABLE [dbo].[Productos]  WITH CHECK ADD  CONSTRAINT [ProveedorProducto] FOREIGN KEY([idProveedor])
REFERENCES [dbo].[Proveedores] ([idProveedor])
GO
ALTER TABLE [dbo].[Productos] CHECK CONSTRAINT [ProveedorProducto]
GO
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [PermisoUsuario] FOREIGN KEY([idPermiso])
REFERENCES [dbo].[Permisos] ([idPermiso])
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [PermisoUsuario]
GO
USE [master]
GO
ALTER DATABASE [Farmacy] SET  READ_WRITE 
GO
