﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FarmacyAPI.Entidades
{
    public class Proveedor
    {
        [Key]
        public int idProveedor { get; set; }
        public string nombre { get; set; }
        public string telefono { get; set; }
        public string direccion { get; set; }
        public string cedula { get; set; }
        public string rnc { get; set; }
        public virtual Producto Productos { get; set; }
    }
}
