﻿using FarmacyAPI.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarmaciAPI.Datos.Mappers
{
    public class ProductosMap : IEntityTypeConfiguration<Producto>
    {
        public void Configure(EntityTypeBuilder<Producto> builder)
        {
            builder.ToTable("Productos").HasKey(x => x.idProducto);
            builder.HasOne(x => x.Proveedores).WithOne(x => x.Productos).HasForeignKey<Proveedor>(x=>x.idProveedor);
        }
    }
}
