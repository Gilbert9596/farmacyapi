﻿using FarmaciAPI.Datos.Mappers;
using FarmacyAPI.Entidades;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarmaciAPI.Datos
{
    public class FarmacyAPIDbContext : DbContext
    {
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Permiso> Permisos { get; set; }
        public DbSet<Proveedor> Proveedores { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<OrderHeader> OrderHeaders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }

        public FarmacyAPIDbContext(DbContextOptions<FarmacyAPIDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new UsuariosMap());
            modelBuilder.ApplyConfiguration(new PermisosMap());
            modelBuilder.ApplyConfiguration(new ProveedoresMap());
            modelBuilder.ApplyConfiguration(new ProductosMap());
            modelBuilder.ApplyConfiguration(new OrderHeaderMap());
            modelBuilder.ApplyConfiguration(new OrderDetailsMap());
        }
    }
}
