﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FarmaciAPI.Datos;
using FarmacyAPI.Entidades;
using FarmacyAPI.Services.Contracts;
using FarmacyAPI.Web.ViewModels.Proveedores;

namespace FarmacyAPI.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProveedoresController : ControllerBase
    {
        private readonly FarmacyAPIDbContext _context;
        private readonly ILoggerService _logger;

        public ProveedoresController(FarmacyAPIDbContext context, ILoggerService logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/Proveedores/GetProveedores
        [HttpGet("[action]")]
        public async Task<IEnumerable<ProveedoresViewModel>> GetProveedores()
        {
            var proveedores = await _context.Proveedores.ToListAsync();

            return proveedores.Select(p => new ProveedoresViewModel
            {
                idProveedor = p.idProveedor,
                nombre = p.nombre,
                rnc = p.rnc,
                cedula = p.cedula,
                direccion = p.direccion,
                telefono = p.telefono
            });
        }

        // GET: api/Proveedores/Proveedor/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> GetProveedor([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var proveedor = await _context.Proveedores.FindAsync(id);

            if (proveedor == null)
            {
                return NotFound();
            }

            return Ok(new ProveedoresViewModel 
            { 
                idProveedor = proveedor.idProveedor,
                nombre = proveedor.nombre,
                rnc = proveedor.rnc,
                cedula = proveedor.cedula,
                direccion = proveedor.direccion,
                telefono = proveedor.telefono
            });
        }

        // PUT: api/Proveedores/UpdateProveedor/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> UpdateProveedor([FromRoute] int id, [FromBody] UpdateProveedorViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model == null)
            {
                return BadRequest();
            }

            var proveedor = await _context.Proveedores.SingleOrDefaultAsync(p=> p.idProveedor == id);

            proveedor.nombre = model.nombre;
            proveedor.direccion = model.direccion;
            proveedor.telefono = model.telefono;
            proveedor.rnc = model.rnc;
            proveedor.cedula = model.cedula;

            try
            {
                await _context.SaveChangesAsync();
                _context.Entry(proveedor).State = EntityState.Modified;
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (!ProveedorExists(id))
                {
                    _logger.logWarn("El proveedor no pudo ser encontrado para la actualización");
                    return BadRequest();
                }
                else
                {
                    string messagee = string.Empty;

                    if (ex.InnerException != null)
                        messagee = ex.InnerException.Message;
                    else
                        messagee = ex.Message;

                    _logger.logError(messagee);
                    return BadRequest();
                }
            }

            return Ok(model);
        }

        // POST: api/Proveedores/InsertProveedor
        [HttpPost("[action]")]
        public async Task<IActionResult> InsertProveedor([FromBody] InsertProveedorViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Proveedor proveedor = new Proveedor
            {
                nombre = model.nombre,
                direccion = model.direccion,
                telefono = model.telefono,
                rnc = model.rnc,
                cedula = model.cedula
            };

            try
            {
                _context.Proveedores.Add(proveedor);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                string messagee = string.Empty;

                if (ex.InnerException != null)
                    messagee = ex.InnerException.Message;
                else
                    messagee = ex.Message;

                _logger.logError(messagee);
                BadRequest();
            }

            return Ok(model);
        }

        // DELETE: api/Proveedores/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> DeleteProveedor([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var proveedor = await _context.Proveedores.FindAsync(id);

            if (proveedor == null)
            {
                return NotFound();
            }

            try
            {
                _context.Proveedores.Remove(proveedor);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                string messagee = string.Empty;

                if (ex.InnerException != null)
                    messagee = ex.InnerException.Message;
                else
                    messagee = ex.Message;

                _logger.logError(messagee);
            }

            return Ok(proveedor);
        }

        private bool ProveedorExists(int id)
        {
            return _context.Proveedores.Any(e => e.idProveedor == id);
        }
    }
}