﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FarmaciAPI.Datos;
using FarmacyAPI.Entidades;
using FarmacyAPI.Web.ViewModels.Orders;
using FarmacyAPI.Services.Contracts;

namespace FarmacyAPI.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly FarmacyAPIDbContext _context;
        private readonly ILoggerService _logger;

        public OrdersController(FarmacyAPIDbContext context, ILoggerService logger)
        {
            _context = context;
            _logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> Venta([FromBody] List<OrdersViewModel> orders)
        {
            var header = orders.FirstOrDefault();

            OrderHeaderViewModel orderHeader = new OrderHeaderViewModel
            {
                idUsuario = header.idUsuario,
                subTotal = header.subTotal,
                totalVenta = header.totalVenta,
                fechaVenta = header.fechaVenta,
                posicion = header.posicion
            };

            await nuevoHeader(orderHeader);

            List<OrderDetailsViewModel> orderDetails = new List<OrderDetailsViewModel>();

            var idHeader = _context.OrderHeaders.LastOrDefault().idOrderHeader;

            foreach (var item in orders)
            {
                OrderDetailsViewModel model = new OrderDetailsViewModel()
                {
                    idOrderHeader = idHeader,
                    idProducto = item.idProducto,
                    cantidad = item.cantidad,
                    precioUnidad = item.precioUnidad,
                    precioTotal = item.precioTotal,
                    itbis = item.itbis
                };

                var producto = await _context.Productos.SingleOrDefaultAsync(x => x.idProducto == item.idProducto);

                if (producto.stock < model.cantidad)
                {

                    var ActualHeader = await _context.OrderHeaders.SingleAsync(x => x.idOrderHeader == idHeader);

                    _context.OrderHeaders.Remove(ActualHeader);

                    return Content("No existe la cantidad suficiente de " + producto.descripcion);
                }
                else
                {
                    producto.stock = producto.stock - item.cantidad;
                }

                await nuevoDetalle(model);
            }

            return Ok();
        }

        public async Task<IActionResult> nuevoHeader(OrderHeaderViewModel model)
        {
            OrderHeader orderHeader = new OrderHeader
            {
                idUsuario = model.idUsuario,
                subTotal = model.subTotal,
                totalVenta = model.totalVenta,
                fechaVenta = model.fechaVenta,
                posicion = model.posicion
            };
            await _context.OrderHeaders.AddAsync(orderHeader);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                string messagee = string.Empty;

                if (ex.InnerException != null)
                    messagee = ex.InnerException.Message;
                else
                    messagee = ex.Message;

                _logger.logError(messagee);

                return BadRequest();
            }
            return Ok();
        }

        public async Task<IActionResult> nuevoDetalle(OrderDetailsViewModel model)
        {
            OrderDetail orderDetail = new OrderDetail()
            {
                idOrderHeader = model.idOrderHeader,
                idProducto = model.idProducto,
                cantidad = model.cantidad,
                precioUnidad = model.precioUnidad,
                precioTotal = model.precioTotal,
                itbis = model.itbis
            };

            await _context.OrderDetails.AddAsync(orderDetail);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                string messagee = string.Empty;

                if (ex.InnerException != null)
                    messagee = ex.InnerException.Message;
                else
                    messagee = ex.Message;

                _logger.logError(messagee);

                var header = await _context.OrderHeaders.SingleAsync(x => x.idOrderHeader == model.idOrderHeader);

                _context.OrderHeaders.Remove(header);

                return BadRequest();
            }
            return Ok();
        }

        private bool OrderDetailExists(int id)
        {
            return _context.OrderDetails.Any(e => e.idOrderDetail == id);
        }

    }
}