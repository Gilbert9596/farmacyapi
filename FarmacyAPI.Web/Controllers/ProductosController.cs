﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FarmaciAPI.Datos;
using FarmacyAPI.Entidades;
using FarmacyAPI.Services.Contracts;
using FarmacyAPI.Web.ViewModels.Productos;

namespace FarmacyAPI.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductosController : ControllerBase
    {
        private readonly FarmacyAPIDbContext _context;
        private readonly ILoggerService _logger;

        public ProductosController(FarmacyAPIDbContext context, ILoggerService logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/Productos/GetProductos
        [HttpGet("[action]")]
        public async Task<IEnumerable<ProductosViewModel>> GetProductos()
        {
            var productos = await _context.Productos.Include(p=>p.Proveedores).ToListAsync();

            return productos.Select(p=> new ProductosViewModel 
            { 
                idProducto = p.idProducto,
                proveedor = p.Proveedores.nombre,
                descripcion = p.descripcion,
                precio = p.precio,
                fechaVencimiento = p.fechaVencimiento,
                stock = p.stock,
                state = p.state
            });
        }

        // GET: api/Productos/GetProducto/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> GetProducto([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var producto = await _context.Productos.Include(p=>p.Proveedores).SingleOrDefaultAsync(p=>p.idProducto == id);

            if (producto == null)
            {
                return NotFound();
            }

            return Ok(new ProductosViewModel 
            { 
                idProducto = producto.idProducto,
                proveedor = producto.Proveedores.nombre,
                descripcion = producto.descripcion,
                precio = producto.precio,
                fechaVencimiento = producto.fechaVencimiento,
                stock = producto.stock,
                state = producto.state
            });
        }

        // PUT: api/Productos/UpdateProducto/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> UpdateProducto([FromRoute] int id, [FromBody] UpdateProductoViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model == null)
            {
                return BadRequest();
            }

            var producto = await _context.Productos.SingleOrDefaultAsync(p => p.idProducto == id);

            producto.idProveedor = model.idProveedor;
            producto.descripcion = model.descripcion;
            producto.precio = model.precio;
            producto.fechaVencimiento = model.fechaVencimiento;

            try
            {
                await _context.SaveChangesAsync();
                _context.Entry(producto).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                if (!ProductoExists(id))
                {
                    _logger.logWarn("El producto no pudo ser encontrado para la actualización");
                    return NotFound();
                }
                else
                {
                    string messagee = string.Empty;

                    if (ex.InnerException != null)
                        messagee = ex.InnerException.Message;
                    else
                        messagee = ex.Message;

                    _logger.logError(messagee);
                    return BadRequest();
                }
            }

            return Ok(model);
        }

        // POST: api/Productos
        [HttpPost("[action]")]
        public async Task<IActionResult> InsertProducto([FromBody] InsertProductoViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model == null)
                return BadRequest(!ModelState.IsValid);

            Producto producto = new Producto
            {
                idProducto = 0,
                idProveedor = model.idProveedor,
                descripcion = model.descripcion,
                precio = model.precio,
                fechaVencimiento = model.fechaVencimiento,
                stock = model.stock,
                state = true
            };

            try
            {
                await _context.Productos.AddAsync(producto);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                if (ProductoExists(producto.idProducto))
                {
                    _logger.logError("El producto ya existe. Favor validar los datos.");
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    string messagee = string.Empty;

                    if (ex.InnerException != null)
                        messagee = ex.InnerException.Message;
                    else
                        messagee = ex.Message;

                    _logger.logError(messagee);
                    return BadRequest();
                }
            }

            return Ok(model);
        }

        // DELETE: api/Productos/DesactivarProducto/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> DesactivarProducto([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var producto = await _context.Productos.FindAsync(id);

            if (producto == null)
            {
                return NotFound();
            }

            producto.state = false;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                string messagee = string.Empty;

                if (ex.InnerException != null)
                    messagee = ex.InnerException.Message;
                else
                    messagee = ex.Message;

                _logger.logError(messagee);
                return BadRequest();
            }

            return Ok();
        }

        // DELETE: api/Productos/ActivarProducto/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> ActivarProducto([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var producto = await _context.Productos.FindAsync(id);

            if (producto == null)
            {
                return NotFound();
            }

            producto.state = true;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                string messagee = string.Empty;

                if (ex.InnerException != null)
                    messagee = ex.InnerException.Message;
                else
                    messagee = ex.Message;

                _logger.logError(messagee);
                return BadRequest();
            }

            return Ok();
        }

        private bool ProductoExists(int id)
        {
            return _context.Productos.Any(e => e.idProducto == id);
        }
    }
}