﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FarmaciAPI.Datos;
using FarmacyAPI.Entidades;
using FarmacyAPI.Web.ViewModels.Permisos;
using FarmacyAPI.Services.Contracts;

namespace FarmacyAPI.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PermisosController : ControllerBase
    {
        private readonly FarmacyAPIDbContext _context;
        private readonly ILoggerService _logger;

        public PermisosController(FarmacyAPIDbContext context, ILoggerService logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/Permisos
        [HttpGet("[action]")]
        public async Task<IEnumerable<PermisosViewModel>> GetPermisos()
        {
            var usuarios = await _context.Permisos.ToListAsync();
            
            return usuarios.Select(p=> new PermisosViewModel
            {
                idPermiso = p.idPermiso,
                descripcion = p.descripcion
            });
        }

        // GET: api/Permisos/Permiso/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Permiso([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var permiso = await _context.Permisos.FindAsync(id);

            if (permiso == null)
            {
                return NotFound();
            }

            return Ok(new PermisosViewModel
            {
                idPermiso = permiso.idPermiso,
                descripcion = permiso.descripcion
            });
        }

        // PUT: api/Permisos/UpdatePermiso/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> UpdatePermiso([FromRoute] int id, [FromBody] UpdatePermisoViewModel permiso)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (permiso == null)
            {
                return BadRequest();
            }
            var value = await _context.Permisos.FindAsync(id);

            value.descripcion = permiso.descripcion; 

            try
            {
                await _context.SaveChangesAsync();
                _context.Entry(permiso).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                if (!PermisoExists(id))
                {
                    return BadRequest();
                }
                else
                {
                    string messagee = string.Empty;

                    if (ex.InnerException != null)
                        messagee = ex.InnerException.Message;
                    else
                        messagee = ex.Message;

                    _logger.logError(messagee);

                    return BadRequest();
                }
            }

            return Ok();
        }

        // POST: api/Permisos/InsertPermiso
        [HttpPost("[action]")]
        public async Task<IActionResult> InsertPermiso([FromBody] NewPermisoViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Permiso permiso = new Permiso
            {
                descripcion = model.descripcion
            };

            _context.Permisos.Add(permiso);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                string messagee = string.Empty;

                if (ex.InnerException != null)
                    messagee = ex.InnerException.Message;
                else
                    messagee = ex.Message;

                _logger.logError(messagee);

                return BadRequest();
            }

            return Ok();
        }

        // DELETE: api/Permisos/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> DeletePermiso([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var permiso = await _context.Permisos.FindAsync(id);

            if (permiso == null)
            {
                return NotFound();
            }

            try
            {
                _context.Permisos.Remove(permiso);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                string messagee = string.Empty;

                if (ex.InnerException != null)
                    messagee = ex.InnerException.Message;
                else
                    messagee = ex.Message;

                _logger.logError(messagee);

                return BadRequest();
            }

            return Ok(permiso);
        }

        private bool PermisoExists(int id)
        {
            return _context.Permisos.Any(e => e.idPermiso == id);
        }
    }
}