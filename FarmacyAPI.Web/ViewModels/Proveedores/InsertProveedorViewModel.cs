﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FarmacyAPI.Web.ViewModels.Proveedores
{
    public class InsertProveedorViewModel
    {
        [Required]
        public string nombre { get; set; }
        [StringLength(11, ErrorMessage = "El telefono solo debe poseer 11 digitos. Favor evite los signos.", MinimumLength = 1)]
        public string telefono { get; set; }
        public string direccion { get; set; }
        [StringLength(11, ErrorMessage = "La cedula solo debe poseer 11 digitos. Favor evite los signos.")]
        public string cedula { get; set; }
        [StringLength(9, ErrorMessage = "El RNC solo debe poseer 9 digitos. Favor evite los signos.")]
        public string rnc { get; set; }
    }
}
