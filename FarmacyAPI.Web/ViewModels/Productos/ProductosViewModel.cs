﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace FarmacyAPI.Web.ViewModels.Productos
{
    public class ProductosViewModel
    {
        [DisplayName("ID")]
        public int idProducto { get; set; }
        [DisplayName("ID")]
        public string proveedor { get; set; }
        public string descripcion { get; set; }
        public decimal precio { get; set; }
        [DisplayName("Existencia")]
        public int stock { get; set; }
        [DisplayName("Fecha de Vencimiento")]
        public DateTime fechaVencimiento { get; set; }
        [DisplayName("Condicion")]
        public bool state { get; set; }
    }
}
