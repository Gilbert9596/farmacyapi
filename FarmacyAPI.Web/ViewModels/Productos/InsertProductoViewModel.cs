﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FarmacyAPI.Web.ViewModels.Productos
{
    public class InsertProductoViewModel
    {
        public int idProveedor { get; set; }
        public string descripcion { get; set; }
        public decimal precio { get; set; }
        public int stock { get; set; }
        public DateTime fechaVencimiento { get; set; }
        public bool state { get; set; }
    }
}
