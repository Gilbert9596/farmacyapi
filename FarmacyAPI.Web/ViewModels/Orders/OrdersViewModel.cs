﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FarmacyAPI.Web.ViewModels.Orders
{
    public class OrdersViewModel
    {
        public int idOrderDetail { get; set; }
        public int idProducto { get; set; }
        public int cantidad { get; set; }
        public decimal precioUnidad { get; set; }
        public decimal precioTotal { get; set; }
        public decimal itbis { get; set; }
        public int idUsuario { get; set; }
        public decimal subTotal { get; set; }
        public decimal totalVenta { get; set; }
        public DateTime fechaVenta { get; set; }
        public int posicion { get; set; }

    }
}
