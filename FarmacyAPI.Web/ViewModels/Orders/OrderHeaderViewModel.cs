﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FarmacyAPI.Web.ViewModels.Orders
{
    public class OrderHeaderViewModel
    {
        public int idUsuario { get; set; }
        public decimal subTotal { get; set; }
        public decimal totalVenta { get; set; }
        public DateTime fechaVenta { get; set; }
        public int posicion { get; set; }
    }
}
