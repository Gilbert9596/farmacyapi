﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FarmacyAPI.Web.ViewModels.Orders
{
    public class OrderDetailsViewModel
    {
        public int idOrderDetail { get; set; }
        public int idOrderHeader { get; set; }
        public int idProducto { get; set; }
        public int cantidad { get; set; }
        public decimal precioUnidad { get; set; }
        public decimal precioTotal { get; set; }
        public decimal itbis { get; set; }
    }
}
