﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FarmacyAPI.Web.ViewModels.Permisos
{
    public class NewPermisoViewModel
    {
        [Key]
        public int idPermiso { get; set; }
        public string descripcion { get; set; }
    }
}
