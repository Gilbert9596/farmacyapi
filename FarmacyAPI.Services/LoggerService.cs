﻿using FarmacyAPI.Services.Contracts;
using NLog;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarmacyAPI.Services
{
    public class LoggerService : ILoggerService
    {
        private static NLog.ILogger _logger = LogManager.GetCurrentClassLogger();

        public void logDebug(string message)
        {
            _logger.Debug(message);
        }

        public void logError(string message)
        {
            _logger.Error(message);
        }

        public void logInfo(string message)
        {
            _logger.Info(message);
        }

        public void logWarn(string message)
        {
            _logger.Warn(message);
        }
    }
}
