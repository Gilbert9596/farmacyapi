﻿using FarmaciAPI.Datos.Mappers;
using FarmacyAPI.Entidades;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarmaciAPI.Datos
{
    public class FarmacyAPIDbContext : DbContext
    {
        DbSet<Usuario> Usuarios { get; set; }
        DbSet<Permiso> Permisos { get; set; }
        DbSet<Proveedor> Proveedores { get; set; }
        DbSet<Producto> Productos { get; set; }
        DbSet<OrderHeader> OrderHeaders { get; set; }
        DbSet<OrderDetail> OrderDetails { get; set; }

        public FarmacyAPIDbContext(DbContextOptions<FarmacyAPIDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new UsuariosMap());
            modelBuilder.ApplyConfiguration(new PermisosMap());
            modelBuilder.ApplyConfiguration(new ProveedoresMap());
            modelBuilder.ApplyConfiguration(new ProductosMap());
            modelBuilder.ApplyConfiguration(new OrderHeaderMap());
            modelBuilder.ApplyConfiguration(new OrderDetailsMap());
        }
    }
}
